﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TravelExpences.Migrations
{
    public partial class AddS : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Comments",
                table: "TripReport",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Comments",
                table: "TripReport");
        }
    }
}
