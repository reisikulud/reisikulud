﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TravelExpences.Models
{
    [AttributeUsage(AttributeTargets.Property)]
    public class DateGreaterThanAttribute : ValidationAttribute
    {
        public DateGreaterThanAttribute(string dateToCompareToFieldName)
        {
            DateToCompareToFieldName = dateToCompareToFieldName;
        }

        private string DateToCompareToFieldName { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            DateTime StartDate = (DateTime)value;

            DateTime EndDate = (DateTime)validationContext.ObjectType.GetProperty(DateToCompareToFieldName).GetValue(validationContext.ObjectInstance, null);

            if (EndDate > StartDate)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult("Lõpukuupäev ei saa olla ennem alguskuupäeva");
            }
        }
    }
}
    