﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TravelExpences.Models
{
    
    public class TripReport 
    {
        public int Id { get; set; }

        //[RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Viga sisestamisel")]
        [Display(Name = "Töötaja nimi")]
        [Required(ErrorMessage = "Kohustuslik väli")]
        public string UserName { get; set; }

        //[RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Viga sisestamisel")]
        [Display(Name = "Reisi põhjus")]
        [Required(ErrorMessage = "Kohustuslik väli")]
        public string TripReason { get; set; }

        //[RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Viga sisestamisel")]
        [Display(Name = "Sihtkoht")]
        [Required(ErrorMessage = "Kohustuslik väli")]
        public string Destination { get; set; }



        [Display(Name = "Alguskuupäev")]
        [Required(ErrorMessage = "Kohustuslik väli")]
        [DataType(DataType.Date)]
        [DateGreaterThan("EndDate")]
        public DateTime StartDate { get; set; }

        [Display(Name = "Lõpukuupäev")]
        [Required(ErrorMessage = "Kohustuslik väli")]
        [DataType(DataType.Date)]
         public DateTime EndDate { get; set; }




        [Display(Name = "Päevade arv")]
        public int DaySum { get; set; } 

        [Display(Name = "Päevarahad")]
        public int? TripDuration { get; internal set; }

        [Display(Name = "Sõidukulud")]
        public double? TransportExpenses { get; set; }

        [Display(Name = "Majutuskulud")]
        public double? AccomodationExpenses { get; set; }

        [Display(Name = "Ööde arv")]
        public int? AccomodationNight { get; set; }

        [Display(Name = "Muud kulud")]
        public double? OtherExpenses { get; set; }

        [Display(Name = "Kuludokumendid")]
        public byte[] File { get; set; }

        public string FileName { get; set; }

        public string FileType { get; set; }

        [Display(Name = "Kommentaarid")]
        //[StringLength(1500)]
        public string Comments { get; set; }

        [Display(Name = "Staatus")]
        public TripStatus TripStatus { get; set; }
        public int TripStatusId { get; set; }
        
        //public bool? ReadyToAccept { get; set; }


        public ApplicationUser User { get; set; }
        public string UserId { get; set; }

    }

    internal class RequiredAttribute : Attribute
    {
        public string ErrorMessage { get; set; }
    }

}
